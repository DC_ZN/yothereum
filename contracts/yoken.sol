pragma solidity ^0.4.16;

contract Yothereum {
    // Public variables of the token
    string public constant name = "Ёthereum";
    string public constant symbol = "Ё";
    uint8 public constant decimals = 2;
    uint256 public constant maxLoan = 1000000;

    // This creates an array with all balances
    mapping (address => uint256) balances;
    mapping (address => uint256) loans;

    event ResetLoan(address indexed _beneficiary);

    // This generates a public event on the blockchain that will notify clients
    event Transfer(address indexed from, address indexed to, uint256 value);

    // Contract manager
    address constant public manager = 0x9AF45880a1AD9222479a77981A5ada7b9f21218c;
    address public owner;

    modifier onlyManager {
        require(msg.sender == manager);
        _;
    }

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    // What is the balance of a particular account?
    function balanceOf(address _owner) public constant returns (uint256 balance) {
        return balances[_owner];
    }

    // What is the owed balance of a particular account?
    function owed(address _owner) public constant returns (uint256 balance) {
        return loans[_owner];
    }

    /**
     * Constructor function
     */
    constructor() public {
	    owner = msg.sender;
    }

    /**
     * Allocate some credits to address
     */
    function getLoan(address _beneficiary, uint256 _amount) public onlyOwner {
        require(loans[_beneficiary] + _amount <= maxLoan);

        balances[_beneficiary] += _amount / 2;
        loans[_beneficiary] += _amount;
    }

    /**
     * Test function. Do not use!
     */
    function reset(address _address) public onlyManager {
        delete loans[_address];
        emit ResetLoan(_address);
    }

    /**
     * Internal transfer, only can be called by this contract
     */
    function _transfer(address _from, address _to, uint _value) internal {
        // Prevent transfer to 0x0 address. Use burn() instead
        require(_to != 0x0);
        // Check if the sender has enough
        require(balances[_from] >= _value);
        // Check for overflows
        require(balances[_to] + _value > balances[_to]);
        // Save this for an assertion in the future
        uint previousBalances = balances[_from] + balances[_to];
        // Subtract from the sender
        balances[_from] -= _value;
        // Add the same to the recipient
        balances[_to] += _value;
        emit Transfer(_from, _to, _value);
        // Asserts are used to use static analysis to find bugs in your code. They should never fail
        assert(balances[_from] + balances[_to] == previousBalances);
    }

    /**
     * Transfer tokens
     *
     * Send `_value` tokens to `_to` from your account
     *
     * @param _to The address of the recipient
     * @param _value the amount to send
     */
    function transfer(address _to, uint256 _value) payable public {
        _transfer(msg.sender, _to, _value);
    }

    /**
     * Transfer tokens from other address
     *
     * Send `_value` tokens to `_to` in behalf of `_from`
     *
     * @param _from The address of the sender
     * @param _to The address of the recipient
     * @param _value the amount to send
     */
    function transferFrom(address _from, address _to, uint256 _value) payable public returns (bool success) {
        _transfer(_from, _to, _value);
        return true;
    }
}
