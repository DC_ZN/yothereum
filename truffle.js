module.exports = {
	networks: {
		private: {
			host: "localhost",
			port: 8545,
			network_id: 13020,
			gas: 4600000,
			from: "0xe278e27e4a9e2dc6d9a52dfcb10e1c538c2087cd"
		}
	}
};
