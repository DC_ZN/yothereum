**This repo contain all about yothereum.**

Conditionally work can be divided into directions:
1.  Contracts writing
    * Yothereum main token;
    * Smart contract for transmission between users;
2.  Security research
    * Bug with WEB wallet;
    * Bug with public function in transmission smart-contract;
    * etc ??? (Ping DSec for information about zn ico-quest);
3.  Web
    * Web wallet version;
4.  Deploy system
    * Docker;
    * Eth-node deployment;
    * All system administration;
    * Quest administration and arbitration;
5.  Testing all components into the one system
    * Working scenarios;
    * Security audit;

---

**Accounts and keyphrases**

1. Sealer: `0x11e4b9301512cbbb62ac6d82728947b283af7259`, `main_account_#123`

2. Fund holder, deployer: `0xe278e27e4a9e2dc6d9a52dfcb10e1c538c2087cd`. `funding_account_#456`

3. Yothereum manager: `0x9af45880a1ad9222479a77981a5ada7b9f21218c`, `qwerty`
